# -*- coding: utf-8 -*-
"""
Created on Tue Oct  3 08:53:25 2023

@author: julien.cotte
"""
import csv
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
def retourne_csv():
    data=[]
    with open(r'taux_activite.csv',newline='')as csvfile:
        spamreader=csv.reader(csvfile,delimiter=';',quotechar='"')
        for row in spamreader:
           data.append(row)
    return data


def write_csv():
    with open(r'taux_activite.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=';', quotechar='"',)

'''def write_nb_habitant(MAX):
    with open(r'taux_activite.csv', 'w', newline='') as csvfile:
        for i in fichier[i]:
            if i[9]>MAX:
                spamwriter = csv.writer(csvfile, delimiter=';', quotechar='"',)  '''

def translate_point():
    temp=[]
    for i in fichier_csv[2:]:
        var=[]
        var.append(int(i[0]))
        for j in i[1:]:
            j=j.replace(',','.')
            j=float(j)
            var.append(j)
        temp.append(var)
    return temp
            
fichier_csv=retourne_csv()
fichier=translate_point()
#print(fichier)


'''Exercice 1  

A partir du tableau des taux d’activité selon le sexe et l’âge, écrire un programme en python qui permet d’extraire les informations suivantes : '''

'''Ecrire une fonction moy_total_annee qui prends en paramètre une année, et permet de renvoyer pour l’année donnée la moyenne d’activité totale pour tous les âges. '''
 
def moy_total_annee(annee):
    for i in fichier:
        if i[0]==int(annee):
            return (i[3]+i[6]+i[9]+i[12]+i[15])/5
#print(moy_total_annee(2010))

'''Ecrire une fonction moy_total_separement qui prends en paramètre une année, et permet de renvoyer pour l’année donnée, la moyenne d’activité totale pour tous les âges des hommes et des femmes séparément. '''

def moy_total_separement_annee(annee):
    dico={'homme':0,'femme':0}
    for i in fichier:
        if i[0]==int(annee):
            dico['homme']=round((i[2]+i[5]+i[8]+i[11]+i[14])/5,2)
            dico['femme']=round((i[1]+i[4]+i[7]+i[10]+i[13])/5,2)
            return dico
#print(moy_total_separement_annee(2021))

'''Ecrire une fonction moy_tranche_age qui permet pour chaque tranche d’âge, de renvoyer la moyenne d’activité totale depuis 1975. '''
def moy_tranche_age():
    dico={'15-24 ans':0,'25-35 ans':0,'36-45 ans':0,'46-64 ans':0,'65 ans ou plus':0}
    for i in fichier:
        dico['15-24 ans']+=i[3]
        dico['25-35 ans']+=i[6]
        dico['36-45 ans']+=i[9]
        dico['46-64 ans']+=i[12]
        dico['65 ans ou plus']+=i[15]
    for i in dico:
        dico[i]=round(dico[i]/(len(fichier)),2)
    return dico
#print(moy_tranche_age())
'''Ecrire une fonction moy_tranche_age_separement qui permet pour chaque tranche d’âge, de renvoyer la moyenne d’activité totale depuis 1975 des hommes et des femmes séparément. '''
def moy_tranche_age_separement():
    dicohomme={'15-24 ans':0,'25-35 ans':0,'36-45 ans':0,'46-64 ans':0,'65 ans ou plus':0}
    dicofemme={'15-24 ans':0,'25-35 ans':0,'36-45 ans':0,'46-64 ans':0,'65 ans ou plus':0}
    
    for i in fichier:
        dicohomme['15-24 ans']+=i[2]
        dicohomme['25-35 ans']+=i[5]
        dicohomme['36-45 ans']+=i[8]
        dicohomme['46-64 ans']+=i[11]
        dicohomme['65 ans ou plus']+=i[14]
        dicofemme['15-24 ans']+=i[1]
        dicofemme['25-35 ans']+=i[4]
        dicofemme['36-45 ans']+=i[7]
        dicofemme['46-64 ans']+=i[10]
        dicofemme['65 ans ou plus']+=i[13]
    for i in dicohomme:
        dicohomme[i]=round(dicohomme[i]/(len(fichier)),2)
    for i in dicofemme:
        dicofemme[i]=round(dicofemme[i]/(len(fichier)),2)
    liste={'homme':dicohomme,'femme':dicofemme}
    return liste
#print(moy_tranche_age_separement())
'''Ecrire une fonction moy_total, qui permet de renvoyer la moyenne d’activité totale depuis 1975. '''
def moy_total():
    total=0
    for i in fichier:
        for j in (3,6,9,12,15):
            total+=i[j]
    return round(total/(len(fichier)*5),2)
#print(moy_total())
'''Ecrire une fonction moy_total_separement, qui permet de renvoyer la moyenne d’activité totale depuis 1975 des hommes et des femmes séparément. '''
def moy_total_separement():
    totalhomme=0
    totalfemme=0
    for i in fichier:
        for j in (2,5,8,11,14):
            totalhomme+=i[j]
        for j in (1,4,7,10,13):
            totalfemme+=i[j]
    totalhomme=round(totalhomme/(len(fichier)*5),2)
    totalfemme=round(totalfemme/(len(fichier)*5),2)
    return {'homme':totalhomme,'femme':totalfemme}
#print(moy_total_separement())
'''Ecrire une fonction moy_intervalle, qui permet pour les intervalles d’années suivantes : 1980-1989, 1990-1999, 2000-2009,2010-2019, de renvoyer la moyenne d’activité totale pour tous les âges. '''
def moy_intervalle():
    dico={'1980-1989':0,'1990-1999':0,'2000-2009':0,'2010-2019':0}
    for i in fichier:
        if i[0] in range(1980,1990) :dico['1980-1989']+=moy_total_annee(i[0])
        if i[0] in range(1990,2000) :dico['1990-1999']+=moy_total_annee(i[0])
        if i[0] in range(2000,2010) :dico['2000-2009']+=moy_total_annee(i[0])
        if i[0] in range(2010,2020) :dico['2010-2019']+=moy_total_annee(i[0])
    for i in dico: dico[i]=round(dico[i]/10,2)
    return dico
#print(moy_intervalle())
'''Ecrire une fonction moy_intervalle_separement, qui permet pour les intervalles d’années suivantes : 1980-1989, 1990-1999, 2000-2009,2010-2019, de renvoyer la moyenne d’activité totale pour tous les âges des hommes et femmes séparément.''' 
def moy_intervalle_separement():
    dicohomme={'1980-1989':0,'1990-1999':0,'2000-2009':0,'2010-2019':0}
    dicofemme={'1980-1989':0,'1990-1999':0,'2000-2009':0,'2010-2019':0}
    for i in fichier:
        if i[0] in range(1980,1990) :
            moyenne=moy_total_separement_annee(i[0])
            dicohomme['1980-1989']+=moyenne['homme']
            dicofemme['1980-1989']+=moyenne['femme']
        if i[0] in range(1990,2000) :
            moyenne=moy_total_separement_annee(i[0])
            dicohomme['1990-1999']+=moyenne['homme']
            dicofemme['1990-1999']+=moyenne['femme']
        if i[0] in range(2000,2010) :
            moyenne=moy_total_separement_annee(i[0])
            dicohomme['2000-2009']+=moyenne['homme']
            dicofemme['2000-2009']+=moyenne['femme']
        if i[0] in range(2010,2020) :
            moyenne=moy_total_separement_annee(i[0])
            dicohomme['2010-2019']+=moyenne['homme']
            dicofemme['2010-2019']+=moyenne['femme']
    for i in dicohomme: dicohomme[i]=round(dicohomme[i]/10,2)
    for i in dicofemme: dicofemme[i]=round(dicofemme[i]/10,2)
    return {'homme':dicohomme,'femme':dicofemme}
#data=moy_intervalle_separement()
#print(data)  
'''En vous basant sur les résultats des données récupérées précédemment, prédire pour les années 2022/2023/2024/2025 le taux d’activité des hommes et femmes séparément pour toutes les tranche d’âges. '''
def liste_moyenne_trie():
    dico={}
    for i in fichier:
        dico[i[0]]=round(moy_total_annee(i[0]),2)
    listeTrie=sorted(dico.items(), key=lambda t: t[0])
    dicotrie={}
    for i in listeTrie:
        dicotrie[i[0]]=i[1]
    return dicotrie
dicoTrie=liste_moyenne_trie()
print(dicoTrie)
'''def liste_croissance():
    dicoCroissance={}
    for i in dicotrie[1:]:
       dicoCroissance[i]=(dicotrie[i]- dicotrie[i-1]) / dicotrie[i-1] x 100
    return dicotrie
liste_croissance()'''
def graphique_moyenne():
    fig, ax = plt.subplots()
    ax.plot(dicoTrie.keys(),dicoTrie.values())
    ax.set_xlabel('année')
    ax.set_ylabel('taux moyen d\'emploi')
    plt.axis([1997,2021,59,65])
graphique_moyenne()
def pente(x1,x2,y1,y2):
    x = (y2 - y1) / (x2 - x1)
    return x
pente_taux=pente(1998,2021,dicoTrie[1998],dicoTrie[2021])
print((0.22*2022)+4.54487)
